# oereb-wms

```
docker run -p 8083:80 -e QGIS_FCGI_MIN_PROCESSES=2 -e QGIS_FCGI_MAX_PROCESSES=2 sogis/oereb-wms:2.0`
```
Min- und Maxprozesse müssen gesetzt werden. Sonst reagiert nach einigen Minuten oder mehreren Requests QGIS-Server nicht mehr. Im `error.log` von Apache steht: "can't apply process slot for /usr/lib/cgi-bin/qgis_mapserv.fcgi".


